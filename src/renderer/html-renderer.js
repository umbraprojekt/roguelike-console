var extend = require("extend");
var DefaultOptions = require("./options/default-options").DefaultOptions;

/**
 * @implements {IRenderer}
 * @constructor
 * @param {TRendererOptions} options
 */
var HtmlRenderer = function(options) {
	/**
	 * @type {TRendererOptions}
	 * @private
	 */
	this._options = extend({}, DefaultOptions, options);

	/**
	 * @type {HTMLDivElement}
	 * @private
	 */
	this._element = document.createElement("DIV");

	/**
	 * A 2D matrix of console cells
	 * @type {Array.<Array.<HTMLDivElement>>}
	 * @private
	 */
	this._cells = [];

	this._element.className = "roguelike-console";
	this._element.style.display = "table";
	this._element.style.whiteSpace = "pre";
	this._element.style.textAlign = "center";
	this._element.style.width = (this._options.width * this._options.cellWidth) + "px";
	this._element.style.height = (this._options.height * this._options.cellHeight) + "px";

	for (var y = 0; y < this._options.height; ++y) {
		var rowElement = this._element.appendChild(document.createElement("DIV"));
		rowElement.className = "roguelike-console-row";
		rowElement.style.width = (this._options.width * this._options.cellWidth) + "px";
		rowElement.style.height = this._options.cellHeight + "px";
		rowElement.style.display = "table-row";

		for (var x = 0; x < this._options.width; ++x) {
			var cellElement = rowElement.appendChild(document.createElement("DIV"));
			cellElement.className = "roguelike-console-cell";
			cellElement.style.width = this._options.cellWidth + "px";
			cellElement.style.height = this._options.cellHeight + "px";
			cellElement.style.display = "table-cell";
			cellElement.style.lineHeight = this._options.cellHeight + "px";
			cellElement.style.fontFamily = this._options.fontFamily;
			cellElement.style.fontWeight = "bold";
			cellElement.style.fontSize = Math.round(this._options.cellHeight * 0.85) + "px";
			this._cells[x] = this._cells[x] || [];
			this._cells[x][y] = cellElement;
		}
	}
};

/**
 * @returns {HTMLDivElement}
 */
HtmlRenderer.prototype.getElement = function() {
	return this._element;
};

/**
 * Render the contents of a console to the screen
 * @param {Console} console
 */
HtmlRenderer.prototype.render = function(console) {
	for (var y = 0; y < Math.min(console.height, this._options.height); ++y) {
		for (var x = 0; x < Math.min(console.width, this._options.width); ++x) {
			var cell = console.getCell(x, y);
			this._cells[x][y].style.background = cell.background;
			this._cells[x][y].style.color = cell.foreground;
			this._cells[x][y].textContent = cell.character;
		}
	}
};

module.exports.HtmlRenderer = HtmlRenderer;
