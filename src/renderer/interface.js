/**
 * @interface IRenderer
 */

/**
 * Render the console contents onto the screen
 * @function
 * @name IRenderer#render
 * @param {Console} console
 */
