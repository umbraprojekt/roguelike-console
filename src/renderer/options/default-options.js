/**
 * @type {TRendererOptions}
 */
module.exports.DefaultOptions = {
	width: 80,
	height: 25,
	cellWidth: 16,
	cellHeight: 16,
	fontFamily: "monospace"
};


