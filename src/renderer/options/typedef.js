/**
 * @typedef {Object} TRendererOptions
 * @property {number} [width=80]
 * @property {number} [height=25]
 * @property {number} [cellWidth=16]
 * @property {number} [cellHeight=16]
 * @property {string} [fontFamily="monospace"]
 */
