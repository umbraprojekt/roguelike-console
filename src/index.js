var RoguelikeConsole = {
	HtmlRenderer: require("./renderer/html-renderer").HtmlRenderer,
	Console: require("./console/console").Console
};

if (window != undefined) {
	window.RoguelikeConsole = RoguelikeConsole;
} else {
	module.exports = RoguelikeConsole;
}
