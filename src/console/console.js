var extend = require("extend");
var DefaultOptions = require("./options/default-options").DefaultOptions;

/**
 * @param {TConsoleOptions} options
 * @constructor
 */
var Console = function(options) {
	/**
	 * @type {TConsoleOptions}
	 * @private
	 */
	this._options = extend({}, DefaultOptions, options);

	/**
	 * @type {Array.<Array.<TConsoleCell>>}
	 * @private
	 */
	this._cells = [];

	for (var y = 0; y < this._options.height; ++y) {
		for (var x = 0; x < this._options.width; ++x) {
			/**
			 * @type {TConsoleCell}
			 */
			var cell = {
				background: this._options.background,
				foreground: this._options.foreground,
				character: ""
			};
			this._cells[x] = this._cells[x] || [];
			this._cells[x][y] = cell;
		}
	}
};

/**
 * @param {number} x
 * @param {number} y
 * @returns {TConsoleCell}
 */
Console.prototype.getCell = function(x, y) {
	return this._cells[x][y];
};

Object.defineProperty(Console.prototype, "width", {
	get: function() {
		return this._options.width;
	}
});

Object.defineProperty(Console.prototype, "height", {
	get: function() {
		return this._options.height;
	}
});

/**
 * @param {number} x
 * @param {number} y
 * @param {string} character
 * @param {string} [foreground]
 * @param {string} [background]
 */
Console.prototype.setCell = function(x, y, character, foreground, background) {
	if (x >= 0 && x < this._options.width && y >= 0 && y < this._options.height) {
		this._cells[x][y].character = character;
		this._cells[x][y].foreground = foreground || this._cells[x][y].foreground;
		this._cells[x][y].background = background || this._cells[x][y].background;
	}
};

/**
 * @param {number} x
 * @param {number} y
 * @param {string} text
 */
Console.prototype.print = function(x, y, text) {
	if (y >= 0 && y < this._options.height) {
		for (var i = 0; i < text.length; ++i) {
			if (x + i >= 0 && x + i < this._options.width) {
				this._cells[x + i][y].character = text[i];
			}
		}
	}
};

module.exports.Console = Console;
