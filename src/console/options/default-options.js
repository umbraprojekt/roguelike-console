/**
 * @type {TConsoleOptions}
 */
module.exports.DefaultOptions = {
	width: 80,
	height: 25,
	foreground: "#c0c0c0",
	background: "#000000"
};


