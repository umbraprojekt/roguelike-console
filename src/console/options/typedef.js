/**
 * @typedef {Object} TConsoleOptions
 * @property {number} [width=80]
 * @property {number} [height=25]
 * @property {string} [foreground="#c0c0c0"]
 * @property {string} [background="#000000"]
 */
