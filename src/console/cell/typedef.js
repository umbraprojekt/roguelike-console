/**
 * @typedef {Object} TConsoleCell
 * @property {string} background
 * @property {string} foreground
 * @property {string} character
 */
