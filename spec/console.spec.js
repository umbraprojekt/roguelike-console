var Console = require("../src/console/console").Console;
var document = require("html-element").document;

describe("Console", function() {
	beforeAll(function() {
		global.document = document;
	});

	afterAll(function() {
		delete global.document;
	});

	it("should initialise", function() {
		// given
		// when
		var con = new Console({});

		// then
		expect(con._cells instanceof Array).toBe(true);
		expect(con._cells.length).toBeGreaterThan(0);
		expect(con._cells[0].length).toBeGreaterThan(0);
		expect(con._cells[0][0].character).toBeDefined();
		expect(con._cells[0][0].foreground).toBeDefined();
		expect(con._cells[0][0].background).toBeDefined();
	});
});
