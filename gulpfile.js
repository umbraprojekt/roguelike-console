var gulp = require("gulp");
var browserify = require("browserify");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var util = require("gulp-util");
var jasmine = require("gulp-jasmine");

gulp.task("js", function() {
	var b = browserify({
		entries: "./src/index.js",
		debug: true
	});

	return b.bundle()
		.pipe(source("bundle.js"))
		.pipe(buffer())
		.pipe(gulp.dest("demo/js/"));
});

gulp.task("test", function() {
	return gulp.src("spec/**/*.spec.js")
		.pipe(jasmine({
			config: require("./spec/support/jasmine.json")
		}));
});
