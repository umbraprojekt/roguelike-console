(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var hasOwn = Object.prototype.hasOwnProperty;
var toStr = Object.prototype.toString;

var isArray = function isArray(arr) {
	if (typeof Array.isArray === 'function') {
		return Array.isArray(arr);
	}

	return toStr.call(arr) === '[object Array]';
};

var isPlainObject = function isPlainObject(obj) {
	if (!obj || toStr.call(obj) !== '[object Object]') {
		return false;
	}

	var hasOwnConstructor = hasOwn.call(obj, 'constructor');
	var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
	// Not own constructor property must be Object
	if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
		return false;
	}

	// Own properties are enumerated firstly, so to speed up,
	// if last one is own, then all properties are own.
	var key;
	for (key in obj) {/**/}

	return typeof key === 'undefined' || hasOwn.call(obj, key);
};

module.exports = function extend() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0],
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if (typeof target === 'boolean') {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	} else if ((typeof target !== 'object' && typeof target !== 'function') || target == null) {
		target = {};
	}

	for (; i < length; ++i) {
		options = arguments[i];
		// Only deal with non-null/undefined values
		if (options != null) {
			// Extend the base object
			for (name in options) {
				src = target[name];
				copy = options[name];

				// Prevent never-ending loop
				if (target !== copy) {
					// Recurse if we're merging plain objects or arrays
					if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
						if (copyIsArray) {
							copyIsArray = false;
							clone = src && isArray(src) ? src : [];
						} else {
							clone = src && isPlainObject(src) ? src : {};
						}

						// Never move original objects, clone them
						target[name] = extend(deep, clone, copy);

					// Don't bring in undefined values
					} else if (typeof copy !== 'undefined') {
						target[name] = copy;
					}
				}
			}
		}
	}

	// Return the modified object
	return target;
};


},{}],2:[function(require,module,exports){
var extend = require("extend");
var DefaultOptions = require("./options/default-options").DefaultOptions;

/**
 * @param {TConsoleOptions} options
 * @constructor
 */
var Console = function(options) {
	/**
	 * @type {TConsoleOptions}
	 * @private
	 */
	this._options = extend({}, DefaultOptions, options);

	/**
	 * @type {Array.<Array.<TConsoleCell>>}
	 * @private
	 */
	this._cells = [];

	for (var y = 0; y < this._options.height; ++y) {
		for (var x = 0; x < this._options.width; ++x) {
			/**
			 * @type {TConsoleCell}
			 */
			var cell = {
				background: this._options.background,
				foreground: this._options.foreground,
				character: ""
			};
			this._cells[x] = this._cells[x] || [];
			this._cells[x][y] = cell;
		}
	}
};

/**
 * @param {number} x
 * @param {number} y
 * @returns {TConsoleCell}
 */
Console.prototype.getCell = function(x, y) {
	return this._cells[x][y];
};

Object.defineProperty(Console.prototype, "width", {
	get: function() {
		return this._options.width;
	}
});

Object.defineProperty(Console.prototype, "height", {
	get: function() {
		return this._options.height;
	}
});

/**
 * @param {number} x
 * @param {number} y
 * @param {string} character
 * @param {string} [foreground]
 * @param {string} [background]
 */
Console.prototype.setCell = function(x, y, character, foreground, background) {
	if (x >= 0 && x < this._options.width && y >= 0 && y < this._options.height) {
		this._cells[x][y].character = character;
		this._cells[x][y].foreground = foreground || this._cells[x][y].foreground;
		this._cells[x][y].background = background || this._cells[x][y].background;
	}
};

/**
 * @param {number} x
 * @param {number} y
 * @param {string} text
 */
Console.prototype.print = function(x, y, text) {
	if (y >= 0 && y < this._options.height) {
		for (var i = 0; i < text.length; ++i) {
			if (x + i >= 0 && x + i < this._options.width) {
				this._cells[x + i][y].character = text[i];
			}
		}
	}
};

module.exports.Console = Console;

},{"./options/default-options":3,"extend":1}],3:[function(require,module,exports){
/**
 * @type {TConsoleOptions}
 */
module.exports.DefaultOptions = {
	width: 80,
	height: 25,
	foreground: "#c0c0c0",
	background: "#000000"
};



},{}],4:[function(require,module,exports){
var RoguelikeConsole = {
	HtmlRenderer: require("./renderer/html-renderer").HtmlRenderer,
	Console: require("./console/console").Console
};

if (window != undefined) {
	window.RoguelikeConsole = RoguelikeConsole;
} else {
	module.exports = RoguelikeConsole;
}

},{"./console/console":2,"./renderer/html-renderer":5}],5:[function(require,module,exports){
var extend = require("extend");
var DefaultOptions = require("./options/default-options").DefaultOptions;

/**
 * @implements {IRenderer}
 * @constructor
 * @param {TRendererOptions} options
 */
var HtmlRenderer = function(options) {
	/**
	 * @type {TRendererOptions}
	 * @private
	 */
	this._options = extend({}, DefaultOptions, options);

	/**
	 * @type {HTMLDivElement}
	 * @private
	 */
	this._element = document.createElement("DIV");

	/**
	 * A 2D matrix of console cells
	 * @type {Array.<Array.<HTMLDivElement>>}
	 * @private
	 */
	this._cells = [];

	this._element.className = "roguelike-console";
	this._element.style.display = "table";
	this._element.style.whiteSpace = "pre";
	this._element.style.textAlign = "center";
	this._element.style.width = (this._options.width * this._options.cellWidth) + "px";
	this._element.style.height = (this._options.height * this._options.cellHeight) + "px";

	for (var y = 0; y < this._options.height; ++y) {
		var rowElement = this._element.appendChild(document.createElement("DIV"));
		rowElement.className = "roguelike-console-row";
		rowElement.style.width = (this._options.width * this._options.cellWidth) + "px";
		rowElement.style.height = this._options.cellHeight + "px";
		rowElement.style.display = "table-row";

		for (var x = 0; x < this._options.width; ++x) {
			var cellElement = rowElement.appendChild(document.createElement("DIV"));
			cellElement.className = "roguelike-console-cell";
			cellElement.style.width = this._options.cellWidth + "px";
			cellElement.style.height = this._options.cellHeight + "px";
			cellElement.style.display = "table-cell";
			cellElement.style.lineHeight = this._options.cellHeight + "px";
			cellElement.style.fontFamily = this._options.fontFamily;
			cellElement.style.fontWeight = "bold";
			cellElement.style.fontSize = Math.round(this._options.cellHeight * 0.85) + "px";
			this._cells[x] = this._cells[x] || [];
			this._cells[x][y] = cellElement;
		}
	}
};

/**
 * @returns {HTMLDivElement}
 */
HtmlRenderer.prototype.getElement = function() {
	return this._element;
};

/**
 * Render the contents of a console to the screen
 * @param {Console} console
 */
HtmlRenderer.prototype.render = function(console) {
	for (var y = 0; y < Math.min(console.height, this._options.height); ++y) {
		for (var x = 0; x < Math.min(console.width, this._options.width); ++x) {
			var cell = console.getCell(x, y);
			this._cells[x][y].style.background = cell.background;
			this._cells[x][y].style.color = cell.foreground;
			this._cells[x][y].textContent = cell.character;
		}
	}
};

module.exports.HtmlRenderer = HtmlRenderer;

},{"./options/default-options":6,"extend":1}],6:[function(require,module,exports){
/**
 * @type {TRendererOptions}
 */
module.exports.DefaultOptions = {
	width: 80,
	height: 25,
	cellWidth: 16,
	cellHeight: 16,
	fontFamily: "monospace"
};



},{}]},{},[4])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvZXh0ZW5kL2luZGV4LmpzIiwic3JjL2NvbnNvbGUvY29uc29sZS5qcyIsInNyYy9jb25zb2xlL29wdGlvbnMvZGVmYXVsdC1vcHRpb25zLmpzIiwic3JjL2luZGV4LmpzIiwic3JjL3JlbmRlcmVyL2h0bWwtcmVuZGVyZXIuanMiLCJzcmMvcmVuZGVyZXIvb3B0aW9ucy9kZWZhdWx0LW9wdGlvbnMuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDdEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ1ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIndXNlIHN0cmljdCc7XG5cbnZhciBoYXNPd24gPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5O1xudmFyIHRvU3RyID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZztcblxudmFyIGlzQXJyYXkgPSBmdW5jdGlvbiBpc0FycmF5KGFycikge1xuXHRpZiAodHlwZW9mIEFycmF5LmlzQXJyYXkgPT09ICdmdW5jdGlvbicpIHtcblx0XHRyZXR1cm4gQXJyYXkuaXNBcnJheShhcnIpO1xuXHR9XG5cblx0cmV0dXJuIHRvU3RyLmNhbGwoYXJyKSA9PT0gJ1tvYmplY3QgQXJyYXldJztcbn07XG5cbnZhciBpc1BsYWluT2JqZWN0ID0gZnVuY3Rpb24gaXNQbGFpbk9iamVjdChvYmopIHtcblx0aWYgKCFvYmogfHwgdG9TdHIuY2FsbChvYmopICE9PSAnW29iamVjdCBPYmplY3RdJykge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxuXG5cdHZhciBoYXNPd25Db25zdHJ1Y3RvciA9IGhhc093bi5jYWxsKG9iaiwgJ2NvbnN0cnVjdG9yJyk7XG5cdHZhciBoYXNJc1Byb3RvdHlwZU9mID0gb2JqLmNvbnN0cnVjdG9yICYmIG9iai5jb25zdHJ1Y3Rvci5wcm90b3R5cGUgJiYgaGFzT3duLmNhbGwob2JqLmNvbnN0cnVjdG9yLnByb3RvdHlwZSwgJ2lzUHJvdG90eXBlT2YnKTtcblx0Ly8gTm90IG93biBjb25zdHJ1Y3RvciBwcm9wZXJ0eSBtdXN0IGJlIE9iamVjdFxuXHRpZiAob2JqLmNvbnN0cnVjdG9yICYmICFoYXNPd25Db25zdHJ1Y3RvciAmJiAhaGFzSXNQcm90b3R5cGVPZikge1xuXHRcdHJldHVybiBmYWxzZTtcblx0fVxuXG5cdC8vIE93biBwcm9wZXJ0aWVzIGFyZSBlbnVtZXJhdGVkIGZpcnN0bHksIHNvIHRvIHNwZWVkIHVwLFxuXHQvLyBpZiBsYXN0IG9uZSBpcyBvd24sIHRoZW4gYWxsIHByb3BlcnRpZXMgYXJlIG93bi5cblx0dmFyIGtleTtcblx0Zm9yIChrZXkgaW4gb2JqKSB7LyoqL31cblxuXHRyZXR1cm4gdHlwZW9mIGtleSA9PT0gJ3VuZGVmaW5lZCcgfHwgaGFzT3duLmNhbGwob2JqLCBrZXkpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBleHRlbmQoKSB7XG5cdHZhciBvcHRpb25zLCBuYW1lLCBzcmMsIGNvcHksIGNvcHlJc0FycmF5LCBjbG9uZSxcblx0XHR0YXJnZXQgPSBhcmd1bWVudHNbMF0sXG5cdFx0aSA9IDEsXG5cdFx0bGVuZ3RoID0gYXJndW1lbnRzLmxlbmd0aCxcblx0XHRkZWVwID0gZmFsc2U7XG5cblx0Ly8gSGFuZGxlIGEgZGVlcCBjb3B5IHNpdHVhdGlvblxuXHRpZiAodHlwZW9mIHRhcmdldCA9PT0gJ2Jvb2xlYW4nKSB7XG5cdFx0ZGVlcCA9IHRhcmdldDtcblx0XHR0YXJnZXQgPSBhcmd1bWVudHNbMV0gfHwge307XG5cdFx0Ly8gc2tpcCB0aGUgYm9vbGVhbiBhbmQgdGhlIHRhcmdldFxuXHRcdGkgPSAyO1xuXHR9IGVsc2UgaWYgKCh0eXBlb2YgdGFyZ2V0ICE9PSAnb2JqZWN0JyAmJiB0eXBlb2YgdGFyZ2V0ICE9PSAnZnVuY3Rpb24nKSB8fCB0YXJnZXQgPT0gbnVsbCkge1xuXHRcdHRhcmdldCA9IHt9O1xuXHR9XG5cblx0Zm9yICg7IGkgPCBsZW5ndGg7ICsraSkge1xuXHRcdG9wdGlvbnMgPSBhcmd1bWVudHNbaV07XG5cdFx0Ly8gT25seSBkZWFsIHdpdGggbm9uLW51bGwvdW5kZWZpbmVkIHZhbHVlc1xuXHRcdGlmIChvcHRpb25zICE9IG51bGwpIHtcblx0XHRcdC8vIEV4dGVuZCB0aGUgYmFzZSBvYmplY3Rcblx0XHRcdGZvciAobmFtZSBpbiBvcHRpb25zKSB7XG5cdFx0XHRcdHNyYyA9IHRhcmdldFtuYW1lXTtcblx0XHRcdFx0Y29weSA9IG9wdGlvbnNbbmFtZV07XG5cblx0XHRcdFx0Ly8gUHJldmVudCBuZXZlci1lbmRpbmcgbG9vcFxuXHRcdFx0XHRpZiAodGFyZ2V0ICE9PSBjb3B5KSB7XG5cdFx0XHRcdFx0Ly8gUmVjdXJzZSBpZiB3ZSdyZSBtZXJnaW5nIHBsYWluIG9iamVjdHMgb3IgYXJyYXlzXG5cdFx0XHRcdFx0aWYgKGRlZXAgJiYgY29weSAmJiAoaXNQbGFpbk9iamVjdChjb3B5KSB8fCAoY29weUlzQXJyYXkgPSBpc0FycmF5KGNvcHkpKSkpIHtcblx0XHRcdFx0XHRcdGlmIChjb3B5SXNBcnJheSkge1xuXHRcdFx0XHRcdFx0XHRjb3B5SXNBcnJheSA9IGZhbHNlO1xuXHRcdFx0XHRcdFx0XHRjbG9uZSA9IHNyYyAmJiBpc0FycmF5KHNyYykgPyBzcmMgOiBbXTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdGNsb25lID0gc3JjICYmIGlzUGxhaW5PYmplY3Qoc3JjKSA/IHNyYyA6IHt9O1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHQvLyBOZXZlciBtb3ZlIG9yaWdpbmFsIG9iamVjdHMsIGNsb25lIHRoZW1cblx0XHRcdFx0XHRcdHRhcmdldFtuYW1lXSA9IGV4dGVuZChkZWVwLCBjbG9uZSwgY29weSk7XG5cblx0XHRcdFx0XHQvLyBEb24ndCBicmluZyBpbiB1bmRlZmluZWQgdmFsdWVzXG5cdFx0XHRcdFx0fSBlbHNlIGlmICh0eXBlb2YgY29weSAhPT0gJ3VuZGVmaW5lZCcpIHtcblx0XHRcdFx0XHRcdHRhcmdldFtuYW1lXSA9IGNvcHk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0Ly8gUmV0dXJuIHRoZSBtb2RpZmllZCBvYmplY3Rcblx0cmV0dXJuIHRhcmdldDtcbn07XG5cbiIsInZhciBleHRlbmQgPSByZXF1aXJlKFwiZXh0ZW5kXCIpO1xyXG52YXIgRGVmYXVsdE9wdGlvbnMgPSByZXF1aXJlKFwiLi9vcHRpb25zL2RlZmF1bHQtb3B0aW9uc1wiKS5EZWZhdWx0T3B0aW9ucztcclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge1RDb25zb2xlT3B0aW9uc30gb3B0aW9uc1xyXG4gKiBAY29uc3RydWN0b3JcclxuICovXHJcbnZhciBDb25zb2xlID0gZnVuY3Rpb24ob3B0aW9ucykge1xyXG5cdC8qKlxyXG5cdCAqIEB0eXBlIHtUQ29uc29sZU9wdGlvbnN9XHJcblx0ICogQHByaXZhdGVcclxuXHQgKi9cclxuXHR0aGlzLl9vcHRpb25zID0gZXh0ZW5kKHt9LCBEZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcblxyXG5cdC8qKlxyXG5cdCAqIEB0eXBlIHtBcnJheS48QXJyYXkuPFRDb25zb2xlQ2VsbD4+fVxyXG5cdCAqIEBwcml2YXRlXHJcblx0ICovXHJcblx0dGhpcy5fY2VsbHMgPSBbXTtcclxuXHJcblx0Zm9yICh2YXIgeSA9IDA7IHkgPCB0aGlzLl9vcHRpb25zLmhlaWdodDsgKyt5KSB7XHJcblx0XHRmb3IgKHZhciB4ID0gMDsgeCA8IHRoaXMuX29wdGlvbnMud2lkdGg7ICsreCkge1xyXG5cdFx0XHQvKipcclxuXHRcdFx0ICogQHR5cGUge1RDb25zb2xlQ2VsbH1cclxuXHRcdFx0ICovXHJcblx0XHRcdHZhciBjZWxsID0ge1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHRoaXMuX29wdGlvbnMuYmFja2dyb3VuZCxcclxuXHRcdFx0XHRmb3JlZ3JvdW5kOiB0aGlzLl9vcHRpb25zLmZvcmVncm91bmQsXHJcblx0XHRcdFx0Y2hhcmFjdGVyOiBcIlwiXHJcblx0XHRcdH07XHJcblx0XHRcdHRoaXMuX2NlbGxzW3hdID0gdGhpcy5fY2VsbHNbeF0gfHwgW107XHJcblx0XHRcdHRoaXMuX2NlbGxzW3hdW3ldID0gY2VsbDtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcblxyXG4vKipcclxuICogQHBhcmFtIHtudW1iZXJ9IHhcclxuICogQHBhcmFtIHtudW1iZXJ9IHlcclxuICogQHJldHVybnMge1RDb25zb2xlQ2VsbH1cclxuICovXHJcbkNvbnNvbGUucHJvdG90eXBlLmdldENlbGwgPSBmdW5jdGlvbih4LCB5KSB7XHJcblx0cmV0dXJuIHRoaXMuX2NlbGxzW3hdW3ldO1xyXG59O1xyXG5cclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KENvbnNvbGUucHJvdG90eXBlLCBcIndpZHRoXCIsIHtcclxuXHRnZXQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0cmV0dXJuIHRoaXMuX29wdGlvbnMud2lkdGg7XHJcblx0fVxyXG59KTtcclxuXHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShDb25zb2xlLnByb3RvdHlwZSwgXCJoZWlnaHRcIiwge1xyXG5cdGdldDogZnVuY3Rpb24oKSB7XHJcblx0XHRyZXR1cm4gdGhpcy5fb3B0aW9ucy5oZWlnaHQ7XHJcblx0fVxyXG59KTtcclxuXHJcbi8qKlxyXG4gKiBAcGFyYW0ge251bWJlcn0geFxyXG4gKiBAcGFyYW0ge251bWJlcn0geVxyXG4gKiBAcGFyYW0ge3N0cmluZ30gY2hhcmFjdGVyXHJcbiAqIEBwYXJhbSB7c3RyaW5nfSBbZm9yZWdyb3VuZF1cclxuICogQHBhcmFtIHtzdHJpbmd9IFtiYWNrZ3JvdW5kXVxyXG4gKi9cclxuQ29uc29sZS5wcm90b3R5cGUuc2V0Q2VsbCA9IGZ1bmN0aW9uKHgsIHksIGNoYXJhY3RlciwgZm9yZWdyb3VuZCwgYmFja2dyb3VuZCkge1xyXG5cdGlmICh4ID49IDAgJiYgeCA8IHRoaXMuX29wdGlvbnMud2lkdGggJiYgeSA+PSAwICYmIHkgPCB0aGlzLl9vcHRpb25zLmhlaWdodCkge1xyXG5cdFx0dGhpcy5fY2VsbHNbeF1beV0uY2hhcmFjdGVyID0gY2hhcmFjdGVyO1xyXG5cdFx0dGhpcy5fY2VsbHNbeF1beV0uZm9yZWdyb3VuZCA9IGZvcmVncm91bmQgfHwgdGhpcy5fY2VsbHNbeF1beV0uZm9yZWdyb3VuZDtcclxuXHRcdHRoaXMuX2NlbGxzW3hdW3ldLmJhY2tncm91bmQgPSBiYWNrZ3JvdW5kIHx8IHRoaXMuX2NlbGxzW3hdW3ldLmJhY2tncm91bmQ7XHJcblx0fVxyXG59O1xyXG5cclxuLyoqXHJcbiAqIEBwYXJhbSB7bnVtYmVyfSB4XHJcbiAqIEBwYXJhbSB7bnVtYmVyfSB5XHJcbiAqIEBwYXJhbSB7c3RyaW5nfSB0ZXh0XHJcbiAqL1xyXG5Db25zb2xlLnByb3RvdHlwZS5wcmludCA9IGZ1bmN0aW9uKHgsIHksIHRleHQpIHtcclxuXHRpZiAoeSA+PSAwICYmIHkgPCB0aGlzLl9vcHRpb25zLmhlaWdodCkge1xyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCB0ZXh0Lmxlbmd0aDsgKytpKSB7XHJcblx0XHRcdGlmICh4ICsgaSA+PSAwICYmIHggKyBpIDwgdGhpcy5fb3B0aW9ucy53aWR0aCkge1xyXG5cdFx0XHRcdHRoaXMuX2NlbGxzW3ggKyBpXVt5XS5jaGFyYWN0ZXIgPSB0ZXh0W2ldO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMuQ29uc29sZSA9IENvbnNvbGU7XHJcbiIsIi8qKlxyXG4gKiBAdHlwZSB7VENvbnNvbGVPcHRpb25zfVxyXG4gKi9cclxubW9kdWxlLmV4cG9ydHMuRGVmYXVsdE9wdGlvbnMgPSB7XHJcblx0d2lkdGg6IDgwLFxyXG5cdGhlaWdodDogMjUsXHJcblx0Zm9yZWdyb3VuZDogXCIjYzBjMGMwXCIsXHJcblx0YmFja2dyb3VuZDogXCIjMDAwMDAwXCJcclxufTtcclxuXHJcblxyXG4iLCJ2YXIgUm9ndWVsaWtlQ29uc29sZSA9IHtcclxuXHRIdG1sUmVuZGVyZXI6IHJlcXVpcmUoXCIuL3JlbmRlcmVyL2h0bWwtcmVuZGVyZXJcIikuSHRtbFJlbmRlcmVyLFxyXG5cdENvbnNvbGU6IHJlcXVpcmUoXCIuL2NvbnNvbGUvY29uc29sZVwiKS5Db25zb2xlXHJcbn07XHJcblxyXG5pZiAod2luZG93ICE9IHVuZGVmaW5lZCkge1xyXG5cdHdpbmRvdy5Sb2d1ZWxpa2VDb25zb2xlID0gUm9ndWVsaWtlQ29uc29sZTtcclxufSBlbHNlIHtcclxuXHRtb2R1bGUuZXhwb3J0cyA9IFJvZ3VlbGlrZUNvbnNvbGU7XHJcbn1cclxuIiwidmFyIGV4dGVuZCA9IHJlcXVpcmUoXCJleHRlbmRcIik7XHJcbnZhciBEZWZhdWx0T3B0aW9ucyA9IHJlcXVpcmUoXCIuL29wdGlvbnMvZGVmYXVsdC1vcHRpb25zXCIpLkRlZmF1bHRPcHRpb25zO1xyXG5cclxuLyoqXHJcbiAqIEBpbXBsZW1lbnRzIHtJUmVuZGVyZXJ9XHJcbiAqIEBjb25zdHJ1Y3RvclxyXG4gKiBAcGFyYW0ge1RSZW5kZXJlck9wdGlvbnN9IG9wdGlvbnNcclxuICovXHJcbnZhciBIdG1sUmVuZGVyZXIgPSBmdW5jdGlvbihvcHRpb25zKSB7XHJcblx0LyoqXHJcblx0ICogQHR5cGUge1RSZW5kZXJlck9wdGlvbnN9XHJcblx0ICogQHByaXZhdGVcclxuXHQgKi9cclxuXHR0aGlzLl9vcHRpb25zID0gZXh0ZW5kKHt9LCBEZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcblxyXG5cdC8qKlxyXG5cdCAqIEB0eXBlIHtIVE1MRGl2RWxlbWVudH1cclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdHRoaXMuX2VsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiRElWXCIpO1xyXG5cclxuXHQvKipcclxuXHQgKiBBIDJEIG1hdHJpeCBvZiBjb25zb2xlIGNlbGxzXHJcblx0ICogQHR5cGUge0FycmF5LjxBcnJheS48SFRNTERpdkVsZW1lbnQ+Pn1cclxuXHQgKiBAcHJpdmF0ZVxyXG5cdCAqL1xyXG5cdHRoaXMuX2NlbGxzID0gW107XHJcblxyXG5cdHRoaXMuX2VsZW1lbnQuY2xhc3NOYW1lID0gXCJyb2d1ZWxpa2UtY29uc29sZVwiO1xyXG5cdHRoaXMuX2VsZW1lbnQuc3R5bGUuZGlzcGxheSA9IFwidGFibGVcIjtcclxuXHR0aGlzLl9lbGVtZW50LnN0eWxlLndoaXRlU3BhY2UgPSBcInByZVwiO1xyXG5cdHRoaXMuX2VsZW1lbnQuc3R5bGUudGV4dEFsaWduID0gXCJjZW50ZXJcIjtcclxuXHR0aGlzLl9lbGVtZW50LnN0eWxlLndpZHRoID0gKHRoaXMuX29wdGlvbnMud2lkdGggKiB0aGlzLl9vcHRpb25zLmNlbGxXaWR0aCkgKyBcInB4XCI7XHJcblx0dGhpcy5fZWxlbWVudC5zdHlsZS5oZWlnaHQgPSAodGhpcy5fb3B0aW9ucy5oZWlnaHQgKiB0aGlzLl9vcHRpb25zLmNlbGxIZWlnaHQpICsgXCJweFwiO1xyXG5cclxuXHRmb3IgKHZhciB5ID0gMDsgeSA8IHRoaXMuX29wdGlvbnMuaGVpZ2h0OyArK3kpIHtcclxuXHRcdHZhciByb3dFbGVtZW50ID0gdGhpcy5fZWxlbWVudC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiRElWXCIpKTtcclxuXHRcdHJvd0VsZW1lbnQuY2xhc3NOYW1lID0gXCJyb2d1ZWxpa2UtY29uc29sZS1yb3dcIjtcclxuXHRcdHJvd0VsZW1lbnQuc3R5bGUud2lkdGggPSAodGhpcy5fb3B0aW9ucy53aWR0aCAqIHRoaXMuX29wdGlvbnMuY2VsbFdpZHRoKSArIFwicHhcIjtcclxuXHRcdHJvd0VsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gdGhpcy5fb3B0aW9ucy5jZWxsSGVpZ2h0ICsgXCJweFwiO1xyXG5cdFx0cm93RWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gXCJ0YWJsZS1yb3dcIjtcclxuXHJcblx0XHRmb3IgKHZhciB4ID0gMDsgeCA8IHRoaXMuX29wdGlvbnMud2lkdGg7ICsreCkge1xyXG5cdFx0XHR2YXIgY2VsbEVsZW1lbnQgPSByb3dFbGVtZW50LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJESVZcIikpO1xyXG5cdFx0XHRjZWxsRWxlbWVudC5jbGFzc05hbWUgPSBcInJvZ3VlbGlrZS1jb25zb2xlLWNlbGxcIjtcclxuXHRcdFx0Y2VsbEVsZW1lbnQuc3R5bGUud2lkdGggPSB0aGlzLl9vcHRpb25zLmNlbGxXaWR0aCArIFwicHhcIjtcclxuXHRcdFx0Y2VsbEVsZW1lbnQuc3R5bGUuaGVpZ2h0ID0gdGhpcy5fb3B0aW9ucy5jZWxsSGVpZ2h0ICsgXCJweFwiO1xyXG5cdFx0XHRjZWxsRWxlbWVudC5zdHlsZS5kaXNwbGF5ID0gXCJ0YWJsZS1jZWxsXCI7XHJcblx0XHRcdGNlbGxFbGVtZW50LnN0eWxlLmxpbmVIZWlnaHQgPSB0aGlzLl9vcHRpb25zLmNlbGxIZWlnaHQgKyBcInB4XCI7XHJcblx0XHRcdGNlbGxFbGVtZW50LnN0eWxlLmZvbnRGYW1pbHkgPSB0aGlzLl9vcHRpb25zLmZvbnRGYW1pbHk7XHJcblx0XHRcdGNlbGxFbGVtZW50LnN0eWxlLmZvbnRXZWlnaHQgPSBcImJvbGRcIjtcclxuXHRcdFx0Y2VsbEVsZW1lbnQuc3R5bGUuZm9udFNpemUgPSBNYXRoLnJvdW5kKHRoaXMuX29wdGlvbnMuY2VsbEhlaWdodCAqIDAuODUpICsgXCJweFwiO1xyXG5cdFx0XHR0aGlzLl9jZWxsc1t4XSA9IHRoaXMuX2NlbGxzW3hdIHx8IFtdO1xyXG5cdFx0XHR0aGlzLl9jZWxsc1t4XVt5XSA9IGNlbGxFbGVtZW50O1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuXHJcbi8qKlxyXG4gKiBAcmV0dXJucyB7SFRNTERpdkVsZW1lbnR9XHJcbiAqL1xyXG5IdG1sUmVuZGVyZXIucHJvdG90eXBlLmdldEVsZW1lbnQgPSBmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcy5fZWxlbWVudDtcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZW5kZXIgdGhlIGNvbnRlbnRzIG9mIGEgY29uc29sZSB0byB0aGUgc2NyZWVuXHJcbiAqIEBwYXJhbSB7Q29uc29sZX0gY29uc29sZVxyXG4gKi9cclxuSHRtbFJlbmRlcmVyLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbihjb25zb2xlKSB7XHJcblx0Zm9yICh2YXIgeSA9IDA7IHkgPCBNYXRoLm1pbihjb25zb2xlLmhlaWdodCwgdGhpcy5fb3B0aW9ucy5oZWlnaHQpOyArK3kpIHtcclxuXHRcdGZvciAodmFyIHggPSAwOyB4IDwgTWF0aC5taW4oY29uc29sZS53aWR0aCwgdGhpcy5fb3B0aW9ucy53aWR0aCk7ICsreCkge1xyXG5cdFx0XHR2YXIgY2VsbCA9IGNvbnNvbGUuZ2V0Q2VsbCh4LCB5KTtcclxuXHRcdFx0dGhpcy5fY2VsbHNbeF1beV0uc3R5bGUuYmFja2dyb3VuZCA9IGNlbGwuYmFja2dyb3VuZDtcclxuXHRcdFx0dGhpcy5fY2VsbHNbeF1beV0uc3R5bGUuY29sb3IgPSBjZWxsLmZvcmVncm91bmQ7XHJcblx0XHRcdHRoaXMuX2NlbGxzW3hdW3ldLnRleHRDb250ZW50ID0gY2VsbC5jaGFyYWN0ZXI7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMuSHRtbFJlbmRlcmVyID0gSHRtbFJlbmRlcmVyO1xyXG4iLCIvKipcclxuICogQHR5cGUge1RSZW5kZXJlck9wdGlvbnN9XHJcbiAqL1xyXG5tb2R1bGUuZXhwb3J0cy5EZWZhdWx0T3B0aW9ucyA9IHtcclxuXHR3aWR0aDogODAsXHJcblx0aGVpZ2h0OiAyNSxcclxuXHRjZWxsV2lkdGg6IDE2LFxyXG5cdGNlbGxIZWlnaHQ6IDE2LFxyXG5cdGZvbnRGYW1pbHk6IFwibW9ub3NwYWNlXCJcclxufTtcclxuXHJcblxyXG4iXX0=
